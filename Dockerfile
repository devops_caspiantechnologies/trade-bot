FROM golang:1.9.2-alpine3.6 AS build

# Install tools required to build the project
# We need to run `docker build --no-cache .` to update those dependencies
RUN apk add --no-cache git \
    && go get -u github.com/golang/dep/cmd/dep

ENV CGO_ENABLED=0 \
    GOOS=linux

# Gopkg.toml and Gopkg.lock lists project dependencies
# These layers are only re-built when Gopkg files are updated
WORKDIR /go/src/trade-bot/
COPY Gopkg.lock Gopkg.toml ./
# Install library dependencies
RUN dep ensure -vendor-only

# Copy all project and build it
# This layer is rebuilt when ever a file has changed in the project directory
COPY . .
RUN go build -a -installsuffix cgo -o /bin/trade-bot

# This results in a single layer image
FROM scratch
COPY --from=build /bin/trade-bot /trade-bot
ENTRYPOINT ["/trade-bot"]
