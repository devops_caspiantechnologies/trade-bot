package main

import (
	"os"
	"os/signal"
	"trade-bot/core"
	"log"
	"syscall"
)

func main() {

	units, err := core.GetConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	interrupt := make(chan os.Signal)
	signal.Notify(interrupt, os.Interrupt, os.Kill,syscall.SIGTERM)

	for i, unit := range units {

		go core.HandleBot(unit)

		log.Printf("bot # %d started",i)
	}

	for {
		<-interrupt
		os.Exit(0)
	}
}
