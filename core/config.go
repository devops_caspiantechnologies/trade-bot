package core

import (
	"os"
	"encoding/json"
	"fmt"
	"sort"
	"github.com/labstack/gommon/log"
)

type Unit struct {
	Url, Port             string
	Email, Password, Pair string
	PriceRange            [2]float64 `json:"price_range"`
	Type, Side            string
	AmountRange           [2]float64 `json:"amount_range"`
	Period                float32
}

var OrderSide = map[string]float64{
	"BUY":  1,
	"SELL": -1,
}

var OrderType = map[string]interface{}{
	"LIMIT":  nil,
	"MARKET": nil,
}

func GetConfiguration() (units []Unit, err error) {

	file, err := os.Open("./configuration.json")
	if err != nil {
		return units, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&units)
	if err != nil {
		return units, err
	}

	if len(units) == 0 {
		return units, fmt.Errorf("empty config")
	}

	syms, err := GetCoinSym(units[0])
	if err != nil {
		log.Fatal(err)
	}

	sort.Sort(sort.StringSlice(syms))

	for _, unit := range units {
		if len(unit.Pair) != 6 {
			return nil, fmt.Errorf("wrong pair")
		}

		if !checkIfHas(syms, unit.Pair[:3]) || !checkIfHas(syms, unit.Pair[3:]) {
			return nil, fmt.Errorf("not accepted pair")
		}

		if unit.Pair[3:] == unit.Pair[:3] {
			return nil, fmt.Errorf("not accepted pair")
		}

		if unit.PriceRange[1] < unit.PriceRange[0] || unit.AmountRange[1] < unit.AmountRange[0] {
			return nil, fmt.Errorf("not valid range")
		}

		if unit.Period == 0 {
			unit.Period = 1
		}

		if _, ok := OrderType[unit.Type]; !ok {
			return nil, fmt.Errorf("wrong order type")
		}

		if _, ok := OrderSide[unit.Side]; !ok {
			return nil, fmt.Errorf("wrong order side")
		}
	}

	return units, nil
}

func checkIfHas(source []string, value string) bool {

	i := sort.StringSlice(source).Search(value)

	return i < len(source) && source[i] == value

}
