package core

import (
	"net/http"
	"encoding/json"
)

func GetCoinSym(u Unit) (syms []string, err error) {

	port := u.Port
	if u.Url == "localhost" {
		port = "8001"
	}

	response, err := http.Get("http://" + u.Url + ":" + port + "/api/v1/trade/coins")
	if err != nil {
		return
	}

	var coins []struct {
		CoinSymbol string `json:"coin_symbol"`
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&coins)
	if err != nil {
		return
	}

	for _, coin := range coins {
		syms = append(syms, coin.CoinSymbol)
	}

	return
}
