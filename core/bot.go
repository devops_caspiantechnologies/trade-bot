package core

import (
	"time"
	"net/http"
	"net/url"
	"encoding/json"
	"github.com/labstack/gommon/log"
	"github.com/dgrijalva/jwt-go"
	"fmt"
	"math/rand"
	"os"
	"bytes"
	"io/ioutil"
	"math"
)

type Tokens struct {
	AccessToken, RefreshToken string
}

type Order struct {
	Pair   string
	Amount string
	Type   string // LIMIT, MARKET
	Price  string
}

const (
	SecretKey = "b_pls397=-s2i9=3!lzg81fh5cv(6y10k(_b+evp=jxwi63hzt"
    precision = 6
	latency = 15 * time.Second
)

func HandleBot(unit Unit) {

	//логинимся
	tokens, expired, err := login(unit)
	if err != nil {
		log.Printf("login error : %v", err)
		os.Exit(1)
	}

	log.Info("login ok")

	for {

		//рефрешим токен если надо
		if !(time.Now().Add(latency)).Before(expired) {
			expired, err = freshTokens(unit, &tokens)
			if err != nil {
				log.Printf("fresh token error : %v", err)
				os.Exit(1)
			}
		}

		//создаем ордер

		amount := (unit.AmountRange[0] + rand.Float64()*(unit.AmountRange[1]-unit.AmountRange[0])) * OrderSide[unit.Side]
		amount = float64(int(amount* math.Pow(10,precision))) / (math.Pow(10,precision))

		price := unit.PriceRange[0] + rand.Float64()*(unit.PriceRange[1]-unit.PriceRange[0])
		price = float64(int(price* math.Pow(10,precision))) / (math.Pow(10,precision))

		order := Order{
			Pair:   unit.Pair,
			Amount: fmt.Sprint(amount),
			Type:   unit.Type,
			Price:  fmt.Sprint(price),
		}

		//отправляем ордер
		ord, err := json.Marshal(order)
		if err != nil {
			log.Printf("create order error : %v", err)
			os.Exit(1)
		}

		client := &http.Client{}

		port := unit.Port
		if unit.Url == "localhost" {
			port = "8001"
		}

		req, err := http.NewRequest("POST", "http://"+unit.Url+":"+port+"/api/v1/trade/order/add", bytes.NewReader(ord))
		req.Header.Add("Authorization", "Bearer "+tokens.AccessToken)
		req.Header.Add("Content-Type", "application/json")
		response, err := client.Do(req)

		if err != nil {
			log.Printf("send order error : %v", err)
			os.Exit(1)
		}

		if response.StatusCode != http.StatusOK {

			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			log.Printf("send order error : %v", string(bodyString))
			os.Exit(1)

		}

		response.Body.Close()

		log.Info("create order : ", order)
		time.Sleep(time.Duration(unit.Period) * time.Second)
	}
}

func login(unit Unit) (tokens Tokens, exp time.Time, err error) {

	port := unit.Port
	if unit.Url == "localhost" {
		port = "8090"
	}

	response, err := http.PostForm("http://"+unit.Url+":"+port+"/api/v1/user/login",
		url.Values{"email": {unit.Email}, "password": {unit.Password}})

	if err != nil {
		return
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&tokens)
	if err != nil {
		return
	}

	exp, err = getExpired(tokens.AccessToken)
	return
}

func freshTokens(unit Unit, tokens *Tokens) (exp time.Time, err error) {

	client := &http.Client{}

	port := unit.Port
	if unit.Url == "localhost" {
		port = "8090"
	}

	req, err := http.NewRequest("GET", "http://"+unit.Url+":"+port+"/api/v1/user/auth/refresh_token", nil)
	req.Header.Add("Authorization", "Bearer "+tokens.RefreshToken)
	response, err := client.Do(req)

	if err != nil {
		return
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&tokens)
	if err != nil {
		return
	}

	exp, err = getExpired(tokens.AccessToken)
	return

}

func getExpired(token string) (expired time.Time, err error) {

	claims := jwt.MapClaims{}
	_, err = jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return
	}

	if exp, ok := claims["exp"]; ok {

		expired = time.Unix(int64(exp.(float64)), 0)
	} else {
		err = fmt.Errorf("token claims error")
		return
	}
	return
}
